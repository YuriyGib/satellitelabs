package task;


import enums.Constants;
import nav.model.ComponentsSatellite;
import nav.model.SatelliteCoordinates;
import nav.model.SatelliteObject;
import nav.model.SatelliteSpeedCoordinates;

import java.util.ArrayList;

public class TaskCoordinates {

    private static Double nuton(Double eps, Double e, Double mk) {
        Double e_curr = 0.0;
        Double e_old = mk;
        while ((Math.abs(e_curr - e_old)) > eps) {
            Double e_temp = e_curr;
            e_curr = e_old + ((mk - e_old + e * Math.sin(e_old)) / (1 - e * Math.cos(e_old)));
            e_old = e_temp;
        }
        return e_curr;
    }

    public static ComponentsSatellite getCoorSat(SatelliteObject satelliteObject, ArrayList<Integer> date, Double t) {
/*
3)Преобразовать время Tpc во время t:
*/
        System.out.println("satelliteObject= " + satelliteObject);
        Double ephemerisTime = satelliteObject.getBroadcastOrbitThree().getEphemerisTime().doubleValue();
        Double sqrtA = satelliteObject.getBroadcastOrbitTwo().getSqrtA().doubleValue();
        Double a = Math.pow(sqrtA, 2);
        if (t == null && !date.isEmpty()) {
            Integer nDay = ephemerisTime.intValue() / 86400;
            Integer hour = date.get(0);
            Integer minute = date.get(1);
            Integer second = date.get(2);
            t = (double) nDay + hour * 3600 + minute * 60 + second;
        }
        System.out.println("t= " + t);
//4)Вычислить момент tk от эпохи времени GPS
        Double toe1 = ephemerisTime;//toe_
        Long toe2 = Math.round(ephemerisTime / 86400) * 86400;//TOE
        if (toe2 == 604800)
            toe2 = 0l;
        Double t_oe_mod = null;
        if (ephemerisTime >= toe2) {
            t_oe_mod = ephemerisTime % 86400;
            t_oe_mod += (1 + (ephemerisTime - toe2) / 86400) * 86400;
            toe1 = t_oe_mod;
        }
        Double tk = t - toe1;
        if (tk > 302400) {
            tk -= 604800;
        }
        if (tk < -302400) {
            tk += 604800;
        }
        System.out.println("tk= " + tk);
// 5) вычислить скорректированное среднее движение
        Double m = Constants.M.doubleValue;
        Double n = Math.sqrt(m / Math.pow(a, 3)) + satelliteObject.getBroadcastOrbitOne().getDeltaN().doubleValue();
        System.out.println("n= " + n);
        // 6) Определить текущее значение средней аномалии Mk в момент времени tk:
        Double Mk = satelliteObject.getBroadcastOrbitOne().getM0().doubleValue() + n * tk;
        System.out.println("Mk= " + Mk);

// 7)Решить итеративным методом Ньютона уравнение Кеплера:
        Double eps = Constants.EPS.doubleValue;
        Double e = satelliteObject.getBroadcastOrbitTwo().getEccentricityOrbit().doubleValue();
        Double Ek2 = nuton(eps, e, Mk);
        System.out.println("e= " + e);
        System.out.println("Ek2= " + Ek2);
// 8) Вычислить истинную производную Ek'
        Double Ek3 = n / (1 - e * Math.cos(Ek2));
        System.out.println("Ek3= " + Ek3);
// 9) Вычислить истинную аномалию
        Double chis = (Math.sqrt(1 - Math.pow(e, 2)) * Math.sin(Ek2));
        Double znam = Math.cos(Ek2) - e;
        Double thiK = Math.atan2(chis, znam);
        System.out.println("thiK= " + thiK);
// 10) Вычислить аргумент широты
        Double phiK = thiK + satelliteObject.getBroadcastOrbitFour().getOmega().doubleValue();
        System.out.println("phiK= " + phiK);
// 11) Вычислить производную аргумента широты
        Double phi2K = Math.sqrt(1 - Math.pow(e, 2)) * Ek3 / (1 - e * Math.cos(Ek2));
        System.out.println("phi2K= " + phi2K);
// 12) Вычислить исправленный аргумент широты
        Double cus = satelliteObject.getBroadcastOrbitTwo().getCus().doubleValue();
        Double cuc = satelliteObject.getBroadcastOrbitTwo().getCuc().doubleValue();
        System.out.println("cus= " + cus);
        Double deltaUk = cuc * Math.cos(2 * phiK) + cus * Math.sin(2 * phiK);
        System.out.println("deltaUk= " + deltaUk);
        Double uK = phiK + deltaUk;
        System.out.println("uK= " + uK);
        Double cosPhiK = Math.cos(2 * phiK);
        System.out.println("cosPhiK= " + cosPhiK);
        Double sinPhiK = Math.sin(2 * phiK);
        System.out.println("sinPhiK= " + sinPhiK);
// 13) Вычислить производную исправленного аргумента широты
        Double uK2 = phi2K * (1 + 2 * (cuc * cosPhiK - cuc * sinPhiK));
        System.out.println("uK2= " + uK2);
// 14) Определить текущее значение исправленного радиус-вектора
        Double crc = satelliteObject.getBroadcastOrbitFour().getCrc().doubleValue();
        Double crs = satelliteObject.getBroadcastOrbitOne().getCrs().doubleValue();
        System.out.println("crc= " + crc);
        Double deltaRK = crc * cosPhiK + crs * sinPhiK;
        System.out.println("deltaRK= " + deltaRK);
        System.out.println("sqrtA= " + sqrtA);
        Double rK = a * (1 - e * Math.cos(Ek2)) + deltaRK;
        System.out.println("rK= " + rK);
// 15) Вычислить производную радиус-вектора
        Double rK2 = a * e * Ek3 * Math.sin(Ek2) + 2 * phi2K *
                (crc * cosPhiK + crs * sinPhiK);
        System.out.println("rK2= " + rK2);
// 16) Определить исправленный угол наклона орбиты
        Double cic = satelliteObject.getBroadcastOrbitThree().getCic().doubleValue();
        System.out.println("cic= " + cic);
        Double cis = satelliteObject.getBroadcastOrbitThree().getCis().doubleValue();
        System.out.println("cis= " + cis);
        Double idot = satelliteObject.getBroadcastOrbitFive().getIdot().doubleValue();
        System.out.println("idot= " + idot);
        Double deltaIk = cic * cosPhiK + cis * sinPhiK;
        System.out.println("deltaIk= " + deltaIk);
        Double Ik = satelliteObject.getBroadcastOrbitFour().getI0().doubleValue() + deltaIk + idot * tk;
        System.out.println("Ik= " + Ik);
// 17) вычисоить производную исправленного угла орбиты
        Double Ik2 = idot + 2 * phi2K * (cis * cosPhiK + cic * sinPhiK);
        System.out.println("Ik2= " + Ik2);
// 18) Вычислить вектор местоположения чпутника в орбитальной плоскости
        Double xPhiK = rK * Math.cos(uK);
        System.out.println("xPhiK= " + xPhiK);
        Double yPhiK = rK * Math.sin(uK);
        System.out.println("yPhiK= " + yPhiK);
// 19) Вычислить производную этого вектора
        Double xPhiK2 = rK2 * Math.cos(uK) - yPhiK * uK2;
        System.out.println("xPhiK2= " + xPhiK2);
        Double yPhiK2 = rK2 * Math.sin(uK) - xPhiK * uK2;
        System.out.println("yPhiK2= " + yPhiK2);
// 20) Вычислить исправленную долготу восходящего узла орбиты
        Double omega3 = Constants.EARTH.doubleValue;
        System.out.println("omega3= " + omega3);
        Double omegaDot = satelliteObject.getBroadcastOrbitFour().getOmegaDOT().doubleValue();
        System.out.println("omegaDot= " + omegaDot);
        Double omegaK = satelliteObject.getBroadcastOrbitThree().getOmega().doubleValue() + (omegaDot - omega3) * tk - omega3 * ephemerisTime;
        System.out.println("omegaK= " + omegaK);
        // 21) вычислить производную долготы восходящего узла
        Double omegaK2 = omegaDot - omega3;
        System.out.println("omegaK2= " + omegaK2);
// 22) вычислить координаты KA на момент времени tk
        //Double xSVK = (xPhiK * Math.cos(omegaK) - yPhiK * Math.cos(Ik) * Math.sin(omegaK))/1000;
        Double xSVK = xPhiK * Math.cos(omegaK) - yPhiK * Math.cos(Ik) * Math.sin(omegaK);
        System.out.println("xSVK= " + xSVK);
        //Double ySVK = (xPhiK * Math.sin(omegaK) + yPhiK * Math.cos(Ik) * Math.cos(omegaK))/1000;
        Double ySVK = xPhiK * Math.sin(omegaK) + yPhiK * Math.cos(Ik) * Math.cos(omegaK);
        System.out.println("ySVK= " + ySVK);
        //Double zSVK = (yPhiK * Math.sin(Ik))/1000;
        Double zSVK = yPhiK * Math.sin(Ik);
        System.out.println("zSVK= " + zSVK);
//Вычислить скорости
        Double xSVK2 = omegaK2 * ySVK + xPhiK2 * Math.cos(omegaK) -
                (yPhiK2 * Math.cos(Ik) - yPhiK * Ik2 * Math.sin(Ik)) * Math.sin(omegaK);
        System.out.println("xSVK2= " + xSVK2);
        Double ySVK2 = omegaK2 * xSVK + xPhiK2 * Math.sin(omegaK) +
                (yPhiK2 * Math.cos(Ik) - yPhiK * Ik2 * Math.sin(Ik)) * Math.cos(omegaK);
        System.out.println("ySVK2= " + ySVK2);
        Double zSVK2 = yPhiK * Ik2 * Math.sin(Ik) + yPhiK2 * Math.sin(Ik);
        System.out.println("zSVK2= " + zSVK2);
        Double dlt_T_r = -4.442807633000001e-10 * e * sqrtA * Math.sin(Ek2);
        SatelliteSpeedCoordinates speedCoordinates = new SatelliteSpeedCoordinates(xSVK2, ySVK2, zSVK2);
        SatelliteCoordinates coordinates = new SatelliteCoordinates(xSVK, ySVK, zSVK);
        return new ComponentsSatellite(coordinates, speedCoordinates, dlt_T_r);
    }
}
