package interf;

import Jama.Matrix;

import java.util.List;

public interface MinSquare {
    double getR(int z, List<Matrix> xSv, Matrix xMatr);
}
