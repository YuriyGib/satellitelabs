import Jama.Matrix;
import convert.ConvertFileRinex;
import enums.ConstantsPath;
import labs.lab2.GetRLab2;
import labs.lab3.GetRLab3;
import nav.convert.Nav;
import nav.model.*;
import obs.convert.RinexObs;
import obs.model.DataObs;
import task.CalcPosition;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import static convert.Matr.getMatr;

public class Main {

    private static Scanner in = new Scanner(System.in);

    public static void main(String[] args) {

        /*List<String> list = ConvertFileRinex.readFileRinex("src/main/java/p0162950.17n", "END OF HEADER");
        ArrayList<SatelliteObject> numberList = Nav.convertToObj(list);
        // ArrayList<Double> curDate = new ArrayList<>();
        System.out.println("Введите номер спутника: ");
        Integer satelliteNumber = in.nextInt();
        List<SatelliteObject> sObjectNum = Nav.getSatelliteNum(numberList, satelliteNumber);
        List<CheckCoordinates> list1 = Nav.cInspectionObj(ConvertFileRinex.readFileRinex(ConstantsPath.FILE_SP3.filePath, "CLK:CMB"));

        for (SatelliteObject sObj : sObjectNum) {
            ArrayList<Integer> date = new ArrayList<>();
            SatellitePRN sPRN = sObj.getSatellitePRN();
            date.add(sPRN.getHour());
            date.add(sPRN.getMinute());
            date.add(sPRN.getSecond().intValue());
            System.out.println("Год: " + sPRN.getYear());
            System.out.print("Месяц: " + sPRN.getMonth());
            System.out.print(" День: " + sPRN.getDay());
            System.out.print(" Часы: " + sPRN.getHour());
            System.out.print(" Минуты: " + sPRN.getMinute());
            System.out.print(" Секунды: " + sPRN.getSecond());
            SatelliteObject satelliteObject = Nav.getSatelliteInTime(numberList, satelliteNumber, date.get(0), date.get(1), sPRN.getSecond().doubleValue(), sPRN.getDay());
            ComponentsSatellite componentsSatellite = TaskCoordinates.getCoorSat(satelliteObject, date,null);
            System.out.println(componentsSatellite.toString());
            SatelliteCoordinates sCoordCheck = Nav.getSatCoord(list1, date.get(0), date.get(1), Double.valueOf(date.get(2)), satelliteNumber);
            SatelliteCoordinates sCoord = componentsSatellite.getSatelliteCoordinates();
            if (sCoordCheck != null)
                System.out.println(Nav.getDelta(sCoord, sCoordCheck));
        }*/
        //List<String> listROF = ConvertFileRinex.readFileRinex("src/main/java/whcr0040.18o", "END OF HEADER");
        List<String> listROF = ConvertFileRinex.readFileRinexStr(ConstantsPath.FILE_OBS.filePath, "END OF HEADER");
        List<DataObs> obsList = RinexObs.convertROF(listROF);
        LocalDateTime dt = LocalDateTime.of(2018, 1, 6, 2, 0, 0);
        List<String> list = ConvertFileRinex.readFileRinex(ConstantsPath.FILE_NAV.filePath, "END OF HEADER");
        ArrayList<SatelliteObject> numberList = Nav.convertToObj(list);
        GetRLab2 getRLab2 = new GetRLab2();
        GetRLab3 getRLab3 = new GetRLab3();
        Matrix x = CalcPosition.getPosition(obsList, numberList, dt, getRLab2);
        if (x != null) {
            System.out.println("Вывод координат:");
            //double[][] arrX  = x.getArray();
            double[][] arrX = getMatr();
            System.out.println("X расч = " + arrX[0][0]);
            System.out.println("Y расч = " + arrX[1][0]);
            System.out.println("Z расч = " + arrX[2][0]);
            String[] coord = ConvertFileRinex.readFileRinexCoord(ConstantsPath.FILE_OBS.filePath, "ANT # / TYPE");
            Double coordX = Double.valueOf(coord[0]);
            Double coordY = Double.valueOf(coord[2]);
            Double coordZ = Double.valueOf(coord[4]);
            System.out.println("X = " + coordX);
            System.out.println("Y = " + coordY);
            System.out.println("Z = " + coordZ);
            System.out.println("DX = " + (arrX[0][0] - coordX));
            System.out.println("DY = " + (arrX[1][0] - coordY));
            System.out.println("DZ = " + (arrX[2][0] - coordZ));
        }
    }
}
