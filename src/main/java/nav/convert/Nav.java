package nav.convert;

import convert.ConvertFileRinex;
import nav.model.*;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Nav {


    public static SatelliteObject getSatelliteInTime(ArrayList<SatelliteObject> satelliteObjects, int numberSatellite, int hour, int minute, double second, int day) {
        for (SatelliteObject satelliteObject : satelliteObjects) {
            SatellitePRN sPRN = satelliteObject.getSatellitePRN();
            if (sPRN.getId() == numberSatellite &&
                    sPRN.getHour() == hour && sPRN.getMinute() == minute &&
                    sPRN.getSecond().doubleValue() == second &&
                    sPRN.getDay() == day) {
                return satelliteObject;
            }
        }
        return null;
    }

    public static List<SatelliteObject> getSatelliteNum(List<SatelliteObject> lst, Integer num) {
        List<SatelliteObject> list = new ArrayList<>();
        for (SatelliteObject sObject : lst) {
            if (sObject.getSatellitePRN().getId().equals(num)) {
                list.add(sObject);
            }
        }
        return list;
    }

    public static List<CheckCoordinates> cInspectionObj(List<String> list) {

        List<CheckCoordinates> checkCoordinates = new ArrayList<>();
        for (int i = 0; i < list.size(); i++) {
            if (list.get(i).equals("*")) {
                List<SatelliteCoordinates> satellites = new ArrayList<>();
                Integer year = Integer.valueOf(list.get(i + 1));
                Integer month = Integer.valueOf(list.get(i + 2));
                Integer day = Integer.valueOf(list.get(i + 3));
                Integer hour = Integer.valueOf(list.get(i + 4));
                Integer minute = Integer.valueOf(list.get(i + 5));
                Double second = Double.valueOf(list.get(i + 6));
                i += 7;
                while (i < list.size() && !list.get(i).equals("*")) {
                    if (list.get(i).contains("PG")) {
                        Integer num = Integer.valueOf(list.get(i).replace("PG", ""));
                        SatelliteCoordinates sCoord = new SatelliteCoordinates(Double.valueOf(list.get(i + 1)), Double.valueOf(list.get(i + 2)), Double.valueOf(list.get(i + 3)));
                        sCoord.setNum(num);
                        i += 3;
                        satellites.add(sCoord);
                    }
                    i++;
                }
                checkCoordinates.add(new CheckCoordinates(year, month, day, hour, minute, second, satellites));
            }
        }
        return checkCoordinates;
    }

    private static CheckCoordinates getCheckCoord(List<CheckCoordinates> lst, Integer hour, Integer minute, Double second) {
        for (CheckCoordinates cCoord : lst) {
            if (cCoord.getHour().equals(hour) && cCoord.getMinute().equals(minute) &&
                    cCoord.getSecond().equals(second)) {
                return cCoord;
            }
        }
        return null;
    }

    public static SatelliteObject getNavTimeID(List<SatelliteObject> nav, LocalDateTime dt, Integer id) {
        for (SatelliteObject navigation : nav) {
            if (navigation.getSatellitePRN().getId().equals(id) &&
                    navigation.getSatellitePRN().getTime().isEqual(dt)) {
                return navigation;
            }
        }
        return null;
    }

    public static SatelliteCoordinates getSatCoord(List<CheckCoordinates> lst, Integer hour, Integer minute, Double second, Integer num) {
        CheckCoordinates cCoord = getCheckCoord(lst, hour, minute, second);
        if (cCoord != null) {
            for (SatelliteCoordinates sCoord : cCoord.getSatellites()) {
                if (sCoord.getNum().equals(num)) {
                    return sCoord;
                }
            }
        }
        return null;
    }

    public static ArrayList<SatelliteObject> convertToObj(List<String> list) {
        ArrayList<TypeSatellite> numberList = new ArrayList<>();
        for (String s : list) {
            if (s.contains("D")) {
                numberList.addAll(getDoubleFromString(s));
            } else {
                if (s.contains(".")) {
                    numberList.add(new TypeSatellite(BigDecimal.valueOf(Double.valueOf(s))));
                } else {
                    numberList.add(new TypeSatellite(Integer.valueOf(s)));
                }
            }
        }
        return getSatelliteObject(numberList);
    }

    private static BigDecimal getAbsValue(Double d1, Double d2) {
        return BigDecimal.valueOf(Math.abs(d1 - d2));
    }

    public static DeltaCoordinates getDelta(SatelliteCoordinates s1, SatelliteCoordinates s2) {
        BigDecimal dx = getAbsValue(s1.getX() / 1000, s2.getX());
        BigDecimal dy = getAbsValue(s1.getY() / 1000, s2.getY());
        BigDecimal dz = getAbsValue(s1.getZ() / 1000, s2.getZ());
        return new DeltaCoordinates(dx, dy, dz);
    }

    private static ArrayList<TypeSatellite> getDoubleFromString(String s) {
        s = ConvertFileRinex.replaceG(s, "D", "E");
        ArrayList<TypeSatellite> arrayList = new ArrayList<>();
        Pattern pattern = Pattern.compile("-?\\d+\\.\\d+(?:E[\\-+]\\d{2})?");
        Matcher matcher = pattern.matcher(s);
        while (matcher.find()) {
            String group = matcher.group();
            arrayList.add(new TypeSatellite(BigDecimal.valueOf(Double.valueOf(group))));
        }
        return arrayList;
    }

    private static ArrayList<SatelliteObject> getSatelliteObject(ArrayList<TypeSatellite> typeSatellites) {
        ArrayList<SatelliteObject> satelliteObjects = new ArrayList<>();
        for (int i = 0; i < typeSatellites.size() / 36; i++) {
            int j = i * 36;
            SatellitePRN satellitePRN = new SatellitePRN(typeSatellites, j);
            BroadcastOrbitOne broadcastOrbitOne = new BroadcastOrbitOne(typeSatellites, j + 10);
            BroadcastOrbitTwo broadcastOrbitTwo = new BroadcastOrbitTwo(typeSatellites, j + 14);
            BroadcastOrbitThree broadcastOrbitThree = new BroadcastOrbitThree(typeSatellites, j + 18);
            BroadcastOrbitFour broadcastOrbitFour = new BroadcastOrbitFour(typeSatellites, j + 22);
            BroadcastOrbitFive broadcastOrbitFive = new BroadcastOrbitFive(typeSatellites, j + 26);
            BroadcastOrbitSix broadcastOrbitSix = new BroadcastOrbitSix(typeSatellites, j + 30);
            BroadcastOrbitSeven broadcastOrbitSeven = new BroadcastOrbitSeven(typeSatellites, j + 34);
            satelliteObjects.add(new SatelliteObject(satellitePRN, broadcastOrbitOne, broadcastOrbitTwo, broadcastOrbitThree, broadcastOrbitFour, broadcastOrbitFive, broadcastOrbitSix, broadcastOrbitSeven));
        }
        return satelliteObjects;
    }
}