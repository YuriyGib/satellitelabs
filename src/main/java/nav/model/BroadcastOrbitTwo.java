package nav.model;

import lombok.Getter;
import java.math.BigDecimal;
import java.util.ArrayList;

@Getter
public class BroadcastOrbitTwo {
    private BigDecimal cuc;
    private BigDecimal eccentricityOrbit;
    private BigDecimal cus;
    private BigDecimal sqrtA;

    public BroadcastOrbitTwo(ArrayList<TypeSatellite> typeSatellites, int i) {
        this.cuc = typeSatellites.get(i).getTypeBigDecimal();
        this.eccentricityOrbit = typeSatellites.get(i + 1).getTypeBigDecimal();
        this.cus = typeSatellites.get(i + 2).getTypeBigDecimal();
        this.sqrtA = typeSatellites.get(i + 3).getTypeBigDecimal();
    }

    @Override
    public String toString() {
        return "BroadcastOrbitTwo{" +
                "cuc=" + cuc +
                ", eccentricityOrbit=" + eccentricityOrbit +
                ", cus=" + cus +
                ", sqrtA=" + sqrtA +
                '}';
    }
}
