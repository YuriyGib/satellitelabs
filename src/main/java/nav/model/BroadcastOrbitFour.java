package nav.model;

import lombok.Getter;
import java.math.BigDecimal;
import java.util.ArrayList;

@Getter
public class BroadcastOrbitFour {
    private BigDecimal i0;
    private BigDecimal crc;
    private BigDecimal omega;
    private BigDecimal omegaDOT;

    public BroadcastOrbitFour(ArrayList<TypeSatellite> typeSatellites, int i) {
        this.i0 = typeSatellites.get(i).getTypeBigDecimal();
        this.crc = typeSatellites.get(i+1).getTypeBigDecimal();
        this.omega = typeSatellites.get(i+2).getTypeBigDecimal();
        this.omegaDOT = typeSatellites.get(i+3).getTypeBigDecimal();
    }

    @Override
    public String toString() {
        return "BroadcastOrbitFour{" +
                "i0=" + i0 +
                ", crc=" + crc +
                ", omega=" + omega +
                ", omegaDOT=" + omegaDOT +
                '}';
    }
}
