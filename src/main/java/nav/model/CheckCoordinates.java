package nav.model;

import lombok.Getter;

import java.util.ArrayList;
import java.util.List;

@Getter
public class CheckCoordinates{
    private Integer year;
    private Integer month;
    private Integer day;
    private Integer hour;
    private Integer minute;
    private Double second;
    private List<SatelliteCoordinates> satellites=new ArrayList<>();

    public CheckCoordinates(Integer year, Integer month, Integer day, Integer hour, Integer minute, Double second, List<SatelliteCoordinates> satellites) {
        this.year = year;
        this.month = month;
        this.day = day;
        this.hour = hour;
        this.minute = minute;
        this.second = second;
        this.satellites = satellites;
    }
}
