package nav.model;

import lombok.Getter;

@Getter
public class SatelliteSpeedCoordinates {
    private Double x;
    private Double y;
    private Double z;

    public SatelliteSpeedCoordinates(Double x, Double y, Double z) {
        this.x = x;
        this.y = y;
        this.z = z;
    }

    @Override
    public String toString() {
        return "SatelliteSpeedCoordinates{" +
                "x=" + x +
                ", y=" + y +
                ", z=" + z +
                '}';
    }
}
