package nav.model;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class SatelliteCoordinates {
    private Double x;
    private Double y;
    private Double z;
    private Integer num;

    public SatelliteCoordinates(Double x, Double y, Double z) {
        this.x = x;
        this.y = y;
        this.z = z;
    }

    @Override
    public String toString() {
        return "SatelliteCoordinates{" +
                "x=" + x +
                ", y=" + y +
                ", z=" + z +
                '}';
    }
}
