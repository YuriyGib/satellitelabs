package nav.model;

import lombok.Getter;
import java.math.BigDecimal;
import java.util.ArrayList;

@Getter
public class BroadcastOrbitFive {
    private BigDecimal idot;
    private BigDecimal codesRangeL2;
    private BigDecimal gpsWeekNumber;
    private BigDecimal dataFlagL2;

    public BroadcastOrbitFive(ArrayList<TypeSatellite> typeSatellites, int i) {
        this.idot = typeSatellites.get(i).getTypeBigDecimal();
        this.codesRangeL2 = typeSatellites.get(i + 1).getTypeBigDecimal();
        this.gpsWeekNumber = typeSatellites.get(i + 2).getTypeBigDecimal();
        this.dataFlagL2 = typeSatellites.get(i + 3).getTypeBigDecimal();
    }

    @Override
    public String toString() {
        return "BroadcastOrbitFive{" +
                "idot=" + idot +
                ", codesRangeL2=" + codesRangeL2 +
                ", gpsWeekNumber=" + gpsWeekNumber +
                ", dataFlagL2=" + dataFlagL2 +
                '}';
    }
}
