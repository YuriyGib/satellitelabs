package nav.model;

import lombok.Getter;

@Getter
public class SatelliteObject {
    private SatellitePRN satellitePRN;
    private BroadcastOrbitOne broadcastOrbitOne;
    private BroadcastOrbitTwo broadcastOrbitTwo;
    private BroadcastOrbitThree broadcastOrbitThree;
    private BroadcastOrbitFour broadcastOrbitFour;
    private BroadcastOrbitFive broadcastOrbitFive;
    private BroadcastOrbitSix broadcastOrbitSix;
    private BroadcastOrbitSeven broadcastOrbitSeven;

    public SatelliteObject(SatellitePRN satellitePRN, BroadcastOrbitOne broadcastOrbitOne, BroadcastOrbitTwo broadcastOrbitTwo, BroadcastOrbitThree broadcastOrbitThree, BroadcastOrbitFour broadcastOrbitFour, BroadcastOrbitFive broadcastOrbitFive, BroadcastOrbitSix broadcastOrbitSix, BroadcastOrbitSeven broadcastOrbitSeven) {
        this.satellitePRN = satellitePRN;
        this.broadcastOrbitOne = broadcastOrbitOne;
        this.broadcastOrbitTwo = broadcastOrbitTwo;
        this.broadcastOrbitThree = broadcastOrbitThree;
        this.broadcastOrbitFour = broadcastOrbitFour;
        this.broadcastOrbitFive = broadcastOrbitFive;
        this.broadcastOrbitSix = broadcastOrbitSix;
        this.broadcastOrbitSeven = broadcastOrbitSeven;
    }

    @Override
    public String toString() {
        return "SatelliteObject{" +
                "satellitePRN=" + satellitePRN + "/n" +
                ", broadcastOrbitOne=" + broadcastOrbitOne + "/n" +
                ", broadcastOrbitTwo=" + broadcastOrbitTwo + "/n" +
                ", broadcastOrbitThree=" + broadcastOrbitThree + "/n" +
                ", broadcastOrbitFour=" + broadcastOrbitFour + "/n" +
                ", broadcastOrbitFive=" + broadcastOrbitFive + "/n" +
                ", broadcastOrbitSix=" + broadcastOrbitSix + "/n" +
                ", broadcastOrbitSeven=" + broadcastOrbitSeven + "/n" +
                '}';
    }
}
