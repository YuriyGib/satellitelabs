package nav.model;

import lombok.Getter;

import java.math.BigDecimal;

@Getter
public class TypeSatellite {
    private BigDecimal typeBigDecimal;
    private Integer typeInteger;

    public TypeSatellite(BigDecimal typeBigDecimal) {
        this.typeBigDecimal = typeBigDecimal;
    }

    public TypeSatellite(Integer typeInteger) {
        this.typeInteger = typeInteger;
    }

    @Override
    public String toString() {
        return "TypeSatellite{" +
                "typeBigDecimal=" + typeBigDecimal +
                ", typeInteger=" + typeInteger +
                '}';
    }
}
