package nav.model;

import lombok.Getter;
import java.math.BigDecimal;
import java.util.ArrayList;

@Getter
public class BroadcastOrbitOne {
    private BigDecimal iode;
    private BigDecimal crs;
    private BigDecimal deltaN;
    private BigDecimal m0;

    public BroadcastOrbitOne(ArrayList<TypeSatellite> typeSatellites, int i) {
        this.iode = typeSatellites.get(i).getTypeBigDecimal();
        this.crs = typeSatellites.get(i + 1).getTypeBigDecimal();
        this.deltaN = typeSatellites.get(i + 2).getTypeBigDecimal();
        this.m0 = typeSatellites.get(i + 3).getTypeBigDecimal();
    }

    @Override
    public String toString() {
        return "BroadcastOrbitOne{" +
                "iode=" + iode +
                ", crs=" + crs +
                ", deltaN=" + deltaN +
                ", m0=" + m0 +
                '}';
    }
}
