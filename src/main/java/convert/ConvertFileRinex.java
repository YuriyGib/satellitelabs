package convert;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class ConvertFileRinex {
    public static List<String> readFileRinex(String path, String stopSearch) {
        try {
            return Files.lines(Paths.get(path), StandardCharsets.UTF_8)
                    .dropWhile(s -> !s.contains(stopSearch))
                    .skip(1)
                    .map(s -> s.split(" "))
                    .flatMap(Arrays::stream)
                    .filter(i -> !i.isEmpty())
                    .collect(Collectors.toList());
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static String[] readFileRinexCoord(String path, String stopSearch) {
        try {
            return Files.lines(Paths.get(path), StandardCharsets.UTF_8)
                    .dropWhile(s -> !s.contains(stopSearch))
                    .skip(1)
                    .findFirst()
                    .map(s -> s.trim().split(" "))
                    .get();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    //построчное считывание файла
    public static List<String> readFileRinexStr(String path, String stopSearch) {
        try {
            return Files.lines(Paths.get(path), StandardCharsets.UTF_8)
                    .dropWhile(s -> !s.contains(stopSearch))
                    .skip(1)
                    .collect(Collectors.toList());
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static String replaceG(String s, String of, String on) {
        return s.replace(of, on);
    }
}
