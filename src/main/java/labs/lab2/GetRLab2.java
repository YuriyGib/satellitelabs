package labs.lab2;

import Jama.Matrix;
import convert.Matr;
import interf.MinSquare;

import java.util.List;

import static convert.Matr.getDouble;

public class GetRLab2 implements MinSquare {
    @Override
    public double getR(int z, List<Matrix> xSv, Matrix xMatr) {
        return Math.sqrt(Math.pow(getDouble(xSv, z, 0) - xMatr.get(0, 0), 2) + Math.pow(getDouble(xSv, z, 1) - xMatr.get(1, 0), 2) + Math.pow(getDouble(xSv, z, 2) - xMatr.get(2, 0), 2));
    }
}
