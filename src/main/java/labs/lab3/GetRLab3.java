package labs.lab3;

import Jama.Matrix;
import interf.MinSquare;

import java.util.List;

import static convert.Matr.getXst;

public class GetRLab3 implements MinSquare {

    @Override
    public double getR(int z, List<Matrix> xSv, Matrix xMatr) {
        double[][] xSt = getXst();
        return Math.sqrt(Math.pow(xSt[0][0] - xMatr.get(0, 0), 2) + Math.pow(xSt[0][1] - xMatr.get(1, 0), 2) + Math.pow(xSt[0][2] - xMatr.get(2, 0), 2));
    }
}
